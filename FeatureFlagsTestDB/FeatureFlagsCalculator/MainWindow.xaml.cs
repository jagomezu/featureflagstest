﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
using FeatureFlagsCalculator.Classes;
using System.Windows;

namespace FeatureFlagsCalculator
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Calculator calculator = new Calculator();
        private char currentOperation = '.';
        private double numberOne;
        private double numberTwo;
        private bool thereIsResult = false;
        private string UserName;

        public MainWindow()
        {
            InitializeComponent();
            numberOne = 0;
            numberTwo = 0;
            ChangeUser();
            lblPermissions.Visibility = Visibility.Hidden;
            VerifyPermissions();
        }

        private string AddNumber(string number)
        {
            string value = txtScreen.Text;

            double numberValue = 0;

            double.TryParse(value, out numberValue);

            if (numberValue == 0)
            {
                if (value.Contains("."))
                {
                    value += number;
                }
                else
                {
                    value = number;
                }
            }
            else
            {
                if (thereIsResult)
                {
                    value = number;
                }
                else
                {
                    value += number;
                }
            }
            thereIsResult = false;

            return value;
        }

        private string AddPoint()
        {
            string value = txtScreen.Text;

            double numberValue = 0;

            double.TryParse(value, out numberValue);

            if (numberValue == 0)
            {
                value = "0.";
            }
            else
            {
                if (!value.Contains("."))
                {
                    value += ".";
                }
            }

            return value;
        }

        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("0");
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("1");
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("2");
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("3");
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("4");
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("5");
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("6");
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("7");
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("8");
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddNumber("9");
        }

        private void btnChangeUser_Click(object sender, RoutedEventArgs e)
        {
            ChangeUser();
            VerifyPermissions();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = "0";
            numberOne = 0;
            numberTwo = 0;
        }

        private void btnDivided_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = '/';
            numberOne = double.Parse(txtScreen.Text);
            txtScreen.Text = "0";
        }

        private void btnEnpowerment_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = 'p';
            numberOne = double.Parse(txtScreen.Text);
            txtScreen.Text = "0";
        }

        private void btnEquals_Click(object sender, RoutedEventArgs e)
        {
            ShowResult();
        }

        private void BtnErase_Click(object sender, RoutedEventArgs e)
        {
            string value = txtScreen.Text;
            if (value.Length > 1)
            {
                value = value.Substring(0, value.Length - 1);
            }
            else
            {
                value = "0";
            }

            txtScreen.Text = value;
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = '-';
            numberOne = double.Parse(txtScreen.Text);
            txtScreen.Text = "0";
        }

        private void btnMultiplication_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = '*';
            numberOne = double.Parse(txtScreen.Text);
            txtScreen.Text = "0";
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = '+';
            numberOne = double.Parse(txtScreen.Text);
            txtScreen.Text = "0";
        }

        private void btnPoint_Click(object sender, RoutedEventArgs e)
        {
            txtScreen.Text = AddPoint();
        }

        private void btnSquare_Click(object sender, RoutedEventArgs e)
        {
            currentOperation = 'r';
            numberOne = double.Parse(txtScreen.Text);
            ShowResult();
        }

        private void ChangeUser()
        {
            UserName = txtUserName.Text;
        }

        private void ShowResult()
        {
            numberTwo = double.Parse(txtScreen.Text);

            double result = 0;

            switch (currentOperation)
            {
                case '+': result = calculator.Plus(numberOne, numberTwo); break;
                case '-': result = calculator.Minus(numberOne, numberTwo); break;
                case '*': result = calculator.Multiplication(numberOne, numberTwo); break;
                case '/': result = calculator.Divide(numberOne, numberTwo); break;
                case 'p': result = calculator.Enpowerment(numberOne, numberTwo); break;
                case 'r': result = calculator.Square(numberOne); break;
            }

            numberOne = result;
            numberTwo = 0;

            txtScreen.Text = result.ToString();

            thereIsResult = true;
        }

        private bool ValidatePermissions(string operation)
        {
            return SplitManager.ValidatePermission(UserName, operation);
        }

        private void VerifyPermissions()
        {
            lblPermissions.Visibility = Visibility.Visible;
            btnEnpowerment.IsEnabled = ValidatePermissions("AndroidTestPot");
            btnSquare.IsEnabled = ValidatePermissions("AndroidTestSqrt");
            lblPermissions.Visibility = Visibility.Hidden;
        }
    }
}