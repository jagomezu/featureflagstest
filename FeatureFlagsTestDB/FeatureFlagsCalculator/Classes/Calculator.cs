﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
using System;

namespace FeatureFlagsCalculator.Classes
{
    public class Calculator
    {
        public double Divide(double numberOne, double numberTwo)
        {
            try
            {
                return numberOne / numberTwo;
            }
            catch
            {
                return 0;
            }
        }

        public double Enpowerment(double numberOne, double numberTwo)
        {
            return Math.Pow(numberOne, numberTwo);
        }

        public double Minus(double numberOne, double numberTwo)
        {
            return numberOne - numberTwo;
        }

        public double Multiplication(double numberOne, double numberTwo)
        {
            return numberOne * numberTwo;
        }

        public double Plus(double numberOne, double numberTwo)
        {
            return numberOne + numberTwo;
        }

        public double Square(double numberOne)
        {
            return Math.Sqrt(numberOne);
        }
    }
}