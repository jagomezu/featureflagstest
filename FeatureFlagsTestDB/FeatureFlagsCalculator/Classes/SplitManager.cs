﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
using Splitio.Services.Client.Classes;
using System;

namespace FeatureFlagsCalculator.Classes
{
    public static class SplitManager
    {
        public static bool ValidatePermission(string userName, string operation)
        {
            /*
             * The SplitClient should be instantiated as a singleton and injected
             * wherever a new feature is being rolled-out.
             */
            var config = new ConfigurationOptions();

            var factory = new SplitFactory("r643vrkj6qf3k3ab94a55q028uotu8t5p7h8", config);

            var splitClient = factory.Client();

            try
            {
                splitClient.BlockUntilReady(10000);
            }
            catch (Exception ex)
            {
                // log & handle
            }

            var treatment = splitClient.GetTreatment(userName, operation);

            return treatment == "on";
        }
    }
}