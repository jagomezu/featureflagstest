﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
require([
    'backbone',
    'appStart',
    'iniApp',

], function (Backbone, appStart, iniApp) {
    appStart.execute();
    console.log("start");
    new iniApp();
    Backbone.history.start();
});