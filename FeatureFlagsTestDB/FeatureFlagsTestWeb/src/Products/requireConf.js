﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
require.config({
    baseUrl: siteUrl + '/Vendors',

    map: {
        '*': {
            'css': 'require-css/css.min'
        }
    },
    paths: {
        // Thrid party libraries (Vendors)
        jquery: 'jquery-3.3.1.min',
        moment: 'moment/moment.min',
        momentDuration: 'moment/moment-duration-format',
        momentLocale: 'moment/locale',
        subroute: 'backbone.subroute.min',
        split: 'split-10.9.1.min',

        bootstrap: 'bootstrap.min',
        // Thrid party libraries (Vendors)
        underscoreBase: 'underscore.min',
        //underscore: 'src/Vendors/underscore.min.js'
        //Custom code (Libs)
        underscore: 'underscoreExtend',
        //underscore: 'underscore.min'
        backbone: 'backbone-min',

        /**************** COMMON *****************/
        // Start App
        appStart: '../Products/appStart',
        app: '../Products/app',
        iniApp: '../Products/ini',

        //Custom code (Libs)
        util: '../Libs/util',

        pathMain: '../Products/Main'

        /******** MODULES / INVENTORY ********/
        // Dashboard
        /* pathCatalogShows: '../Products/Modules/CatalogShows',
         pathHeader: '../Products/Modules/Common/Header',
         pathCard: '../Products/Modules/CatalogShows/Card',
         pathShow: '../Products/Modules/CatalogShows/Show',
         pathEpisode: '../Products/Modules/CatalogShows/Show/Season',
         pathFonts: '../Fonts'*/
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        },
        bootstrap: {
            exports: 'bootstrap',
            deps: ['jquery']
        },
        underscoreBase: {
            exports: '_'
        },
        underscore: {
            deps: ['underscoreBase'],
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'momentLocale/es': {
            deps: ['moment']
        },
        momentDuration: {
            deps: ['moment']
        }
    }
});

console.log(siteUrl + '/Vendors/');