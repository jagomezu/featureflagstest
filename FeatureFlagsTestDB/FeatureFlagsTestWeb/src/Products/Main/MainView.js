﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
define([
    'jquery',
    'underscore',
    'backbone',
    'text!pathMain/templateMain.html',
    'split',
    'css!pathMain/css.css' 
    /*,
    'util',
    'pathCatalogShows/collectionShows',
    'pathCard/CardView',
    'css!pathCatalogShows/css.css'*/
], function ($, _, Backbone, templateMain, splitio/*, util, collectionShows, CardView*/) {
    return Backbone.View.extend({
        el: 'section.main',

        events: {
            //'click #evalueButton': 'evaluar',
            'click .number': 'eval',
            'change #selectUser': 'selectUser'
        },

        initialize: function (options) {
            //enlace a el template de _ underscore la funcion detect_scroll
            _.bindAll(this,

                'show',
                'evaluar',
                'eval'

            );
            this.$el.html(templateMain);
            //this.show();
            
        },

        show: function (user) {

            user = user || 'Jorge';

            var template = _.template($('#templateCalc').html());
            //this.$el.html(templateMain);
            this.$el.find('#containerCalc').append(template);

            var factory = splitio({
                core: {
                    authorizationKey: 'ghav3jk22qmt2o9t43oqvaqmhfrk31h1nacu',
                    //key: 'Jorge' // unique identifier for your user
                    //key: 'ricardo' // unique identifier for your user
                    key: user // unique identifier for your user
                }
            });

            var client = factory.client();
            var that = this;
            client.on(client.Event.SDK_READY, function () {
                var treatmentPot = client.getTreatment('AndroidTestPot');
                var treatmentSqrt = client.getTreatment('AndroidTestSqrt');
                
                that.$el.find('#labelPot').text(treatmentPot);
                that.$el.find('#labelSqrt').text(treatmentSqrt);

                if (treatmentPot === 'on') {
                    that.$el.find('#buttonPot').show();
                    that.$el.find('#labelPot').removeClass('textRed');

                } else if (treatmentPot === 'off') {
                    that.$el.find('#buttonPot').hide();
                    that.$el.find('#labelPot').addClass('textRed');

                }

                if (treatmentSqrt === 'on') {
                    that.$el.find('#buttonSqrt').show();
                    that.$el.find('#labelSqrt').removeClass('textRed');
                    
                } else if (treatmentSqrt === 'off') {
                    that.$el.find('#buttonSqrt').hide();
                    that.$el.find('#labelSqrt').addClass('textRed');

                }

            });
        },

        evaluar: function(){
            cadena=document.getElementById('calc').value;
            
            division=cadena.split('÷');
            // alert(cadena);
            adiv=division[0];
            mult1=adiv.split("x");
            amult=1;
            for(i = 0; i<mult1.length; i++){

                sumar = mult1[i].split('+');

                restar = sumar[0].split('-');
                res1 = restar[0];
        // alert(res1);
                for (j = 1; j < restar.length; j++) {
                    res1 = res1 - restar[j];
                }
                sum1 = res1;
                for (j = 1; j < sumar.length; j++) {
                    restar = sumar[j].split('-');
                    res1 = restar[0];
                    for (k = 1; k < restar.length; k++) {
                        res1 = res1 - restar[k];
                    }
                    sum1 = sum1 * 1 + res1 * 1;
                }
                amult = amult * sum1;

                
                



            // alert(amult);
            }
    // alert(amult);
        adiv = amult;
        for (i = 1; i < division.length; i++) {
            adivn = division[i];
            multn = adivn.split("x");
            sumar = multn[0].split('+');

            restar = sumar[0].split('-');
            res1 = restar[0];
            // alert(res1);
            for (j = 1; j < restar.length; j++) {
                res1 = res1 - restar[j];
            }
            sumn = res1;
            for (h = 1; h < sumar.length; h++) {
                restar = sumar[h].split('-');
                res1 = restar[0];
                for (k = 1; k < restar.length; k++) {
                    res1 = res1 - restar[k];
                }
                sumn = sumn + res1 * 1;
            }
            adiv = adiv / sumn;
            for (j = 1; j < multn.length; j++) {

                sumar = multn[i].split('+');
                restar = sumar[0].split('-');
                res1 = restar[0];
                // alert(res1);
                for (k = 1; k < restar.length; k++) {
                    res1 = res1 - restar[k];
                }
                sumn = res1;

                for (h = 1; h < sumar.length; h++) {
                    restar = sumar[h].split('-');
                    res1 = restar[0];
                    for (k = 1; k < restar.length; k++) {
                        res1 = res1 - restar[k];
                    }
                    sumn = sumn * 1 + res1 * 1;
                }
                adiv = adiv * sumn;
            }

        }
        document.getElementById('calc').value = adiv;
        },

        eval: function () {

            var cadena = document.getElementById('calc').value;
            if (cadena.charAt(0) === '√' && cadena.length > 1) {
                document.getElementById('calc').value = Math.sqrt(cadena.charAt(1));
                return;
            }
            else if (cadena.length > 2 && cadena.charAt(1) === '√') {
                document.getElementById('calc').value = cadena.charAt(0) * Math.sqrt(cadena.charAt(2));
                return;
            }

            if (cadena.charAt(0) === '^' && cadena.length > 1) {
                document.getElementById('calc').value = 'Error';
                return;
            }
            else if (cadena.length > 2 && cadena.charAt(1) === '^') {
                document.getElementById('calc').value = Math.pow(cadena.charAt(0),cadena.charAt(2));
                return;
            }

            this.evaluar();
            
        },

        selectUser: function (e) {
            
            var $element = $(e.currentTarget);
            console.log('selectUser ' + $element.children("option:selected").val());
            var user = $element.children("option:selected").val();
            this.$el.find('#containerCalc').empty();
            this.show(user);
            

        }
    });
});