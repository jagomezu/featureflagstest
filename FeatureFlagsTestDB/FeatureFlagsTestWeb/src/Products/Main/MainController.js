﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
define([
    'backbone',
    'util',
    'underscore',
    'pathMain/MainView'
], function (backbone, util, _, MainView) {
    return backbone.Router.extend({
        initialize: function () {
            /*_.bindAll(this,
                'getDashboardTemplate',
                'getFolderListCollection',
                'getFolderListCollectionSuccess',
                'showDashboard');
                */
        },

        execute: function () {
            util.printDebug('executeControllerMain');
            if (_.isEmpty(this.mainView)) {
                this.mainView = new MainView();
            }
            //this.showsView = new viewShows({ searchString: searchString });
            this.mainView.show();
        }
    });
});