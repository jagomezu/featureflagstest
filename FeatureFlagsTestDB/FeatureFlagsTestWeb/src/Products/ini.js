﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
define([
    'underscore',
    'backbone',
    'util'
    //'pathHeader/view'
], function (_, backbone, util/*, header*/) {
    return backbone.Router.extend({
        //shows: null,
        initialize: function () {
            //Cargar cabeceras
            // header = new header();
        },
        routes: {
            '*default': 'invokeMainView'
        },

        invokeMainView: function () {
            util.printDebug('invokeMainView');

            var that = this;
            require([
                'pathMain/MainController'
            ], function (MainController) {
                try {
                    if (!that.main) {
                        that.main = new MainController();
                    }
                    that.main.execute();
                } finally {
                    //MetroPreLoader.close();
                    util.printDebug('finally');
                }
            });

            /*var that = this;
            require([
                'pathCatalogShows/controllerCatalogShows'
            ], function (controllerCatalogShows) {
                try {
                    if (!that.shows) {
                        that.shows = new controllerCatalogShows();
                    }
                    that.shows.execute();
                } finally {
                    //MetroPreLoader.close();
                    util.printDebug('finally');
                }
            });*/
            /* console.log('b');
             var that = this;
             require(['pathCatalogShows/ini'], function (configShow) {
                 if (!that.shows) {
                     console.log('a');
                     that.shows = new configShow('');
                 }
                 console.log('c');
                 header.hideSearch();
             });*/
        }
    });
});