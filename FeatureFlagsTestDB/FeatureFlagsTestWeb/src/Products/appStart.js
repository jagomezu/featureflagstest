﻿define([
    'jquery',
    'util',
    'moment',
    //Iconografia por medio de Fuentes
    'css!pathFonts/jorgeFont/jorgeFont.css',
], function ($, util, moment) {
    return {
        execute: function () {
            console.log('execute');
            window.util = util;

            util.printDebug('printDebug');

            if (window.language !== 'es' && window.language !== 'en') {
                window.language = 'en';
            }
            moment.lang(window.language);

        }   
        };
});