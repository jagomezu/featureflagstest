﻿define([
    'jquery',
    'underscore',
    'backbone',
    'moment'
], function ($, _, Backbone, moment) {
    var util = (function ($, window, document, undefined) {
        var debug = true;

        return {
            /*
             * creamos nuestro método para tener un mismo punto de acceso en cada debuging que se hace en la aplicación
             */
            printDebug: function (message, element) {
                try {
                    //si se ha activado el debugger de la aplicación.
                    if (debug && typeof (console) !== 'undefined' && console.log) {
                        //mostramos el mensaje
                        if (message)
                            console.log(message);
                        //mostramos el elemento
                        if (element)
                            console.log(element);
                    }
                } catch (e) {
                    console.error(e);
                }
            },
            printError: function (exception, element) {
                //si se ha activado el debugger de la aplicación.
                if (debug && typeof (console) !== 'undefined' && console.error()) {
                    //mostramos el mensaje
                    if (exception)
                        console.error(exception);
                    //mostramos el elemento
                    if (element)
                        console.error(element);
                }
            }
        };
    })($, window, document);
    return util;
});