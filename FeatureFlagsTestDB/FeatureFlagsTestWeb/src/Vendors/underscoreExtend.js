﻿// <copyright company="Aranda Software">
// © Todos los derechos reservados
// </copyright>
_.templateSettings = {
    escape: /\{\{(.+?)\}\}/g,
    interpolate: /\[\[(.+?)\]\]/g,
    evaluate: /\{#(.+?)#\}/g
};

_.isJson = function (str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

_.templates = function (options, element, template) {
    if (!_.isJson(options)) {
        options = JSON.parse(JSON.stringify(options));
    }

    options.forEach(function (option) {
        optionHtml = _.template(template, option);
        element.append(optionHtml);
    });

    return element.html();
};

_.templateEmpty = function (templateString) {
    var emptyReplace = {};
    return _.template(templateString, emptyReplace);
};